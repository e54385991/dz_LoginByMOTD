#include <sourcemod>
#include <SteamWorks>
#include <webfix>


ConVar DZPluginURL;
char sDZPluginURL[512];
char sHTMLRedict[512];
ConVar DZPluginAPIKey;
ConVar HTMLRedict;
char sAPIKEY[128];


public void OnPluginStart()
{
	RegConsoleCmd("sm_dz", Command_DISCUZ_Login);
	
	
	DZPluginURL = CreateConVar("discuz_login_plugin_url", "http://bbs.93x.net/plugin.php?id=xnet_steam_openid:GameLogin_authcode", "");
	HTMLRedict = CreateConVar("discuz_login_plugin_redict", "http://bbs.93x.net/source/plugin/xnet_steam_openid/redirect.html", "");
	DZPluginAPIKey = CreateConVar("discuz_login_apikey", "apipassword", "");

	DZPluginURL.AddChangeHook(OnConVarChanged);
	HTMLRedict.AddChangeHook(OnConVarChanged);
	DZPluginAPIKey.AddChangeHook(OnConVarChanged);

	
	HTMLRedict.GetString(sHTMLRedict,512);
	WebFix_SetRedirectUrl(sHTMLRedict);
	
	AutoExecConfig(true, "DiscuzLogin");
}


public void OnConVarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if(convar == DZPluginURL)
		DZPluginURL.GetString(sDZPluginURL,512);
	else if(convar == HTMLRedict)
	{
		DZPluginURL.GetString(sHTMLRedict,512);
		WebFix_SetRedirectUrl(sHTMLRedict);
	}
	else if(convar == DZPluginAPIKey)
		DZPluginAPIKey.GetString(sAPIKEY,128);
}

public Action Command_DISCUZ_Login(int client, int args)
{
	if(client && IsClientInGame(client))
	{
		char authid64[45];
		GetClientAuthId(client,AuthId_SteamID64,authid64,45);
		
		DiscuzLogin_SteamWork(GetClientUserId(client),authid64,sAPIKEY);
	}
	
	return Plugin_Handled;
}



void DiscuzLogin_SteamWork(int userid,const char[] SteamID64,const char[] APIKEY)
{
	char sRequest[512];
	
	FormatEx(sRequest, sizeof(sRequest), sDZPluginURL);
	Handle hRequest = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, sRequest);
	
	SteamWorks_SetHTTPRequestGetOrPostParameter(hRequest, "steam", SteamID64);
	SteamWorks_SetHTTPRequestGetOrPostParameter(hRequest, "key", APIKEY);
	SteamWorks_SetHTTPRequestGetOrPostParameter(hRequest, "act", "getpw");
	
	if (!hRequest || !SteamWorks_SetHTTPCallbacks(hRequest, LoginDiscuz_CallBack) || !SteamWorks_SetHTTPRequestContextValue(hRequest, userid) || !SteamWorks_SendHTTPRequest(hRequest))
	{
		delete(hRequest);
		return;
	}
}


public int LoginDiscuz_CallBack(Handle hRequest, bool bFailure, bool bRequestSuccessful, EHTTPStatusCode eStatusCode, int userid)
{
	if (!bFailure && bRequestSuccessful && eStatusCode == k_EHTTPStatusCode200OK)
	{
		SteamWorks_GetHTTPResponseBodyCallback(hRequest, APIWebResponse_CallBack,userid);
	}
	
	if(hRequest != null) delete hRequest;
}


public int APIWebResponse_CallBack(char[] sData, int userid)
{
	int client = GetClientOfUserId(userid);
	TrimString(sData);
	
	char sURL[1024];
	
	if(client && IsClientInGame(client))
	{
		char authid64[45];
		GetClientAuthId(client,AuthId_SteamID64,authid64,45);
		
		if(StrContains(sData,"Error",false) == -1)
		{
			Format(sURL,512,"%s&steam=%s&act=login&logincode=%s",sDZPluginURL,authid64,sData);
			
			WebFix_OpenUrl(client, "DiscuzLogin", sURL,false);
		}
		else if(StrContains(sData,"Error 101",false) != -1)
		{
			Format(sURL,1024,"%s&steam=%s&act=reg",sDZPluginURL,authid64);
			
			WebFix_OpenUrl(client, "DiscuzLogin", sURL,false);
		}else PrintToChat(client,"错误 %s",sData);
	}
}

