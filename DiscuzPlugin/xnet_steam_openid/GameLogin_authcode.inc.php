<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}
global $_G;
$key = "apipassword";
$authkey = "yourpassword";
$page = "/";
if (is_array($_GET) && count($_GET) > 2) {
    if (isset($_GET["steam"])) {
        $steamid = daddslashes($_GET['steam']);
    } else {
        exit("Error No SteamID");
    }
    if (isset($_GET["act"])) {
        $Act = $_GET["act"];
       
    } else {
        exit("Error No Action");
    }
    if ($Act == 'login') {
        if (isset($_GET["logincode"])) {
            $LoginCode = x_decode($_GET["logincode"], $authkey);
        } else {
            exit("Error No logincode");
        }
    }
    if ($Act == 'getpw') {
        if (isset($_GET["key"])) {
            $APIKEY = $_GET["key"];
        } else {
            exit("Error No APIKEY");
        }
    }
    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    }
} else {
    exit("Error GET");
}
//Server
if ($APIKEY == $key && $Act == 'getpw') {
    if ($steamid64 = DB::result_first("select steamID64 from " . DB::table('steam_users') . " where steamID64='" . $steamid . "'")) {
        exit(MakeAuthCode($steamid64, $authkey));
    } else {
        exit("Error 101 Not Bind Steam");
    }
}
if ($Act == 'reg') {
    if (!$_G["uid"]) {
        showmessage("你的Steam没有绑定论坛.请先完成注册 或者 绑定...", '/member.php');
    } else {
        showmessage("正在登陆...", $page);
    }
}
if ($Act == 'login') {
    $SecureCode = AuthDeCode($LoginCode, $authkey);
    if ($SecureCode != $steamid || $SecureCode == '' || !$SecureCode || !$LoginCode) {
        exit("Error 102 {$SecureCode} {$LoginCode}");
        //认证失败
    }
    if ($uid = DB::result_first("select uid from " . DB::table('steam_users') . " where steamID64='" . $steamid . "'")) {
        if ($uid > 0) {
            if (!$_G["uid"]) {
                loaducenter();
                //UC用户中心载入
                $member = getuserbyuid($uid, 1);
                dsetcookie('auth', authcode("{$member['password']}\t{$member['uid']}", 'ENCODE'), 1800);
                showmessage("正在登陆...", $page);
            } else {
                loaducenter();
                $member = getuserbyuid($uid, 1);
                dsetcookie('auth', authcode("{$member['password']}\t{$member['uid']}", 'ENCODE'), 1800);
                showmessage("正在登陆...", $page);
            }
        }
    }
}
//Make .
// function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0)
function MakeAuthCode($steamID64, $skey)
{
    global $_G;
    $PD = authcode($steamID64, 'ENCODE', $skey, 1800);
    return x_encode($PD);
}
function AuthDeCode($scode, $skey)
{
    global $_G;
    $PD = authcode($scode, 'DECODE', $skey, 1800);
    return $PD;
}
function x_encode($str)
{
    $src = array("/", "+", "=");
    $dist = array("_a", "_b", "_c");
    $old = base64_encode($str);
    $new = str_replace($src, $dist, $old);
    return $new;
}
function x_decode($str)
{
    $src = array("_a", "_b", "_c");
    $dist = array("/", "+", "=");
    $old = str_replace($src, $dist, $str);
    $new = base64_decode($old);
    return $new;
}